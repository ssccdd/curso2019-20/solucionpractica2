[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# SEGUNDA PRÁCTICA

## Resolución con monitores

Para la solución de la práctica se utilizará como herramienta de concurrencia el desarrollo de monitores. El número de monitores necesarios deberá justificarse adecuadamente por parte del alumno. 

## Desarrollo de la práctica

1. **Instrucciones para la entrega:** En el espacio de docencia virtual de la asignatura hay definida una actividad donde se encuentran las instrucciones de entrega de la práctica. Hay que clonar este repositorio como punto de partida de la práctica. Luego cada alumno deberá personalizarlo, siguiendo las instrucciones de entrega, para completar el desarrollo de la práctica.

2. **Resolución teórica:** Consiste en realizar el análisis y diseño del problema. Para ello hay que especificar los TDAs (Clases) que sean necesarias, con sus atributos (estado) y sus métodos (comportamiento). Hay que justificar las decisiones que se tomen sobre todo por qué se da una determinada responsabilidad a una determinada clase. 

La documentación estará recogida en el fichero **README**, en formato [markdown](https://es.wikipedia.org/wiki/Markdown), en el repositorio de entrega. Un porcentaje de plagio mayor al 10% tendrá como consecuencia la no evaluación de la práctica.

3. **Implementación:** Para la implementación hay que utilizar las herramientas de concurrencia que nos proporciona Java para la definición de monitores. Para la ejecución de las tareas que conforman la práctica hay que utilizar el marco de ejecución [`Executor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) con alguna de las clases que implementan esta interfaces.

El código se someterá también a la detección de copias y un porcentaje mayor del 10% tendrá como consecuencia la no evaluación de la práctica.

**PROBLEMA A RESOLVER**

En una granja de rendering (render farm) se generan **Escenas** compuestas por **Fotogramas** que luego se renderizan. Todo el proceso se realiza concurrentemente. Las características del proceso son:

- Los **Fotogramas** tienen:
	- Identificador único para cada fotograma.
	- Un tiempo de cálculo aleatorio entre **MIN_TIEMPO_FOTOGRAMA** y **MIN_TIEMPO_FOTOGRAMA** + **VARIACION_TIEMPO** segundos. Este tiempo se calcula cuando se construye el fotograma y se conserva durante el resto de la vida del fotograma. Es el tiempo que tardará un renderizador de escenas en renderizar este fotograma.

- Las **Escenas** tienen:
	- Identificador único para cada escena.
	- Prioridad **ALTA** o **BAJA** que indica si se atenderán antes o después por los renderizadores de escenas. Sólo cuando no hay ninguna escena prioritaria a la espera de ser renderizada se atienden las escenas no prioritarias.
	- Conjunto de fotogramas con tamaño aleatorio entre **MIN_NUM_FOTOGRAMAS** y **MIN_NUM_FOTOGRAMAS** + **VARIACION_NUM_FOTOGRAMAS**.
	- El tiempo que se tarda en renderizar una escena es la suma del tiempo de cada fotograma mas un tiempo fijo de: **TIEMPO_FINALIZACION_ESCENA**.

Las tareas mínimas necesarias para la resolución del problema serán:

- **Generadores de escenas** que recopilan todo lo necesario para renderizar una escena, la construyen y la ponen a disposición de los renderizadores. Un generador tarda un tiempo aleatorio entre **MIN_TIEMPO_GENERACION** y **MIN_TIEMPO_GENERACION** + **VARIACION_TIEMPO** segundos en generar la escena.

- **Renderizadores de escenas** cuyo funcionamiento es:
	- Tomar una escena de las que hay disponibles. Siempre se elige la que tiene mas prioridad. Sólo cuando no hay escenas con prioridad **ALTA** se elige una escena con prioridad **BAJA**.
	- Renderizarla. Una **Escena** no está completa hasta que no se han completado todos sus **Fotogramas**.
	- Cuando una escena ha sido renderizada deberá almacenarse adecuadamente.
	
- Se definirán los monitores necesarios para la comunicación entre las tareas de la práctica:
	- Hay una capacidad máxima para las escenas definido por la constante **MAX_ESCENAS_EN_ESPERA**. Es decir, los Generadores de escenas deberán sincronizarse no sobrepasar esa capacidad máxima. Los Renderizadores también deberán sincronizarse hasta que tengan escenas disponibles.

- **Sistema**:
	- Se construirán **NUM_GENERADORES** Generadores de escenas.
	- Se construirán **NUM_RENDERIZADORES** Renderizadores de escenas.
	- Cada Generador de escenas construirá un número de escenas comprendido entre **MIN_NUM_ESCENAS** y **MIN_NUM_ESCENAS** + **VARIACION_NUM_ESCENAS**. (Cada escena tiene un número aleatorio de fotogramas tal y como se describe mas arriba).
	- Cuando los generadores han generado todas sus escenas, terminan su ejecución.
	- Cuando no quedan escenas pendientes y los generadores han terminado su ejecución, los renderizadores terminan su ejecución.
	- Al finalizar la ejecución de todas las tareas, el programa mostrará:
	- El número total de escenas procesadas.
	- El tiempo total empleado en **Renderizar** todas las escenas.
	- Para cada escena:
		- La hora en que se generó.
		- La hora en que un Renderizador comenzó su procesamiento.
		- Los fotogramas que la componen y la duración de cada uno.
		- La hora en la que el Renderizador terminó su procesamiento.
		- El tiempo total empleado en el procesamiento de la escena.


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE1ODkzOTI3NF19
-->