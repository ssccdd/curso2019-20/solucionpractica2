/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.solucionpractica2;

import java.util.Random;

/**
 * Constantes, tipo enumerado y generador de numeros aleatorios necesarios para 
 * la ejecucion de la aplicacion
 * @author fconde
 */
public class Constantes {
    
    // - Generador de numeros aleatorio ----------------------------------------
    public static final Random aleatorio = new Random();

    // - Tipo enumerado --------------------------------------------------------
    public enum Prioridad {
        BAJA(0), ALTA(1);

        private final int valor;

        private Prioridad(int valor) {
            this.valor = valor;
        }
        
        public static Prioridad getPrioridadAleatoria() {
            int valorAleatorio = aleatorio.nextInt(MAX_VALOR_PRIORIDAD);
            return Prioridad.values()[valorAleatorio];
        }
    }
    
    // - Constantes ------------------------------------------------------------
    public static final int MAX_VALOR_PRIORIDAD = 2;

    public static final int VARIACION_TIEMPO = 2;   
    
    public static final int MIN_TIEMPO_FOTOGRAMA = 2;
    public static final int TIEMPO_FINALIZACION_ESCENA = 2;
    
    public static final int MIN_NUM_FOTOGRAMAS = 3;
    public static final int VARIACION_NUM_FOTOGRAMAS = 3;
    
    public static final int MIN_NUM_ESCENAS = 3;
    public static final int VARIACION_NUM_ESCENAS = 3;
    
    public static final int MIN_TIEMPO_GENERACION = 3;
    
    public static final int NUM_GENERADORES = 4;
    public static final int NUM_RENDERIZADORES = 3;
    
    public static final int MAX_ESCENAS_EN_ESPERA = 4;
}